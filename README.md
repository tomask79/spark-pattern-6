# Hortonworks Spark Certification test pattern, question 6. #

*6th is to submit a spark job on yarn.*

**Task:** Let's build a jar containing apache spark job consuming commandline argument containing path to the HDFS file.
Spark job will then printout the file content.

## 1. Preparing of the Spark job.

Main class performing the mentioned task:

    import org.apache.spark.SparkContext
    import org.apache.spark.SparkConf;

    object SampleSparkJob {
  
      def print(line: String):Unit={
        println("Printing inside: "+line);
      }
  
      def main(args: Array[String]):Unit={

        val conf = new SparkConf();
        conf.setAppName("Sample Spark Scala Application");
    
        val sc = new SparkContext(conf);

        val fileToRead = args(0);

        val fileRDD = sc.textFile(fileToRead);

        fileRDD.foreach(line=>print(line));

        System.exit(0);
      }  
    }

I've attached whole SBT project. To build it:

* install SBT at your PC. On my MacPro I did [the following](http://macappstore.org/sbt/).
* in the root directory run: **sbt assembly**. This will give you the jar file.
* If you use [Hortonworks Sandbox](https://hortonworks.com/tutorial/hortonworks-sandbox-guide/section/1/), then scp the file into sandbox:

Run the following:

    [tomask79:scala-2.10 tomask79$ scp -P 2222 sample-spark-project-assembly-1.0.jar root@localhost:/root
    root@localhost's password: 
    sample-spark-project-assembly-1.0.jar                                   100% 2804     1.6MB/s   00:00    

## 2. Running the jar on Spark Yarn 

Let's launch jar on yarn in the [client mode](https://stackoverflow.com/questions/20793694/what-is-yarn-client-mode-in-spark) and put the path to file as parameter.    
The same Yarn Mode we were training at Hortonworks Spark workshop in Prague. The same I expect in the exam.

    [root@sandbox ~]# spark-submit --class SampleSparkJob --master yarn-client sample-spark-project-assembly-1.0.jar /tests/inputFile2.txt
    
At http://<YourSandbox-host>:8088 figure out ID of your spark job (application_1535919221399_0027 in my case) and let's view the Job output:

    [root@sandbox ~]# yarn logs -applicationId application_1535919221399_0027

Job output was:

    LogType:stdout
    Log Upload Time:Po Zář 03 08:25:19 +0000 2018
    LogLength:145
    Log Contents:
    Printing inside: Second input file, first line.
    Printing inside: Second input file, second line.
    Printing inside: Second input file, third line.

    End of LogType:stdout

Let's test another command parameter to the Spark Job:

    [root@sandbox ~]# spark-submit --class SampleSparkJob --master yarn-client sample-spark-project-assembly-1.0.jar /tests/inputFile.txt
    
Log output:    

    [root@sandbox ~]# yarn logs -applicationId application_1535919221399_0028

    LogType:stdout
    Log Upload Time:Po Zář 03 08:28:31 +0000 2018
    LogLength:109
    Log Contents:
    Printing inside: This is first row.
    Printing inside: This is second row.
    Printing inside: This is third row.

    End of LogType:stdout
    
Btw '--master yarn-client' is an spark 1.x syntax. Following spark-submit is Spark 2.x equivalent:

    [root@sandbox ~]# spark-submit --class SampleSparkJob --master yarn --deploy-mode client sample-spark-project-assembly-1.0.jar /tests/inputFile.txt

Getting logs:

    [root@sandbox ~]# yarn logs -applicationId application_1535919221399_0029

    LogType:stdout
    Log Upload Time:Po Zář 03 08:43:25 +0000 2018
    LogLength:109
    Log Contents:
    Printing inside: This is first row.
    Printing inside: This is second row.
    Printing inside: This is third row.

    End of LogType:stdout

Best regards

Tomas

PS: Only one test question to go.