import org.apache.spark.SparkContext
import org.apache.spark.SparkConf;

object SampleSparkJob {
  
  def print(line: String):Unit={
    println("Printing inside: "+line);
  }
  
  def main(args: Array[String]):Unit={

    val conf = new SparkConf();
    conf.setAppName("Sample Spark Scala Application");
    
    val sc = new SparkContext(conf);

    val fileToRead = args(0);

    val fileRDD = sc.textFile(fileToRead);

    fileRDD.foreach(line=>print(line));

    System.exit(0);
  }  
}
